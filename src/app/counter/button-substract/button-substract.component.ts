import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-button-substract',
  templateUrl: './button-substract.component.html',
  styleUrls: ['./button-substract.component.scss']
})
export class ButtonSubstractComponent {

  @Input()
  counter: number = 0;

  @Output()
  newValue: EventEmitter<number> = new EventEmitter();

  @Output()
  negativeCounter: EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  substract(): void {
    if (this.counter - 1 < 0) {
      this.negativeCounter.emit(true);
    } else {
      this.counter -= 1;
      this.newValue.emit(this.counter);
    }
  }
}
