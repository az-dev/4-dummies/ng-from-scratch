import { TestBed, ComponentFixture, fakeAsync, tick } from '@angular/core/testing';
import { ButtonSubstractComponent } from './button-substract.component';

describe('ButtonSubstractComponent', () => {

  let component: ButtonSubstractComponent;
  let fixture: ComponentFixture<ButtonSubstractComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ButtonSubstractComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonSubstractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should start the counter on zero.', () => {
    expect(component.counter).toBe(0);
  });

  it('should emit an error after calling the method substract() once.', () => {
    let value_emitted = false;

    component.negativeCounter.subscribe( value => {
      value_emitted = value;
    });

    component.substract();

    expect(value_emitted).toBeTrue();
  });

  it('should emit no value after calling the method substract() once.', fakeAsync(() => {
    let value_emitted;

    component.newValue.subscribe( value => {
      value_emitted = value;
    });

    component.substract();

    tick(2000);

    expect(value_emitted).toBeUndefined();
  }));

});
