import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-button-add',
  templateUrl: './button-add.component.html',
  styleUrls: ['./button-add.component.scss']
})
export class ButtonAddComponent {

  @Input()
  counter: number = 0;

  @Output()
  newValue: EventEmitter<number> = new EventEmitter();

  @Output()
  negativeCounter: EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  add(): void {
    if (this.counter + 1 >= 0) {
      this.negativeCounter.emit(false);
    }

    this.counter += 1;

    this.newValue.emit(this.counter);
  }
}
