import { TestBed, ComponentFixture } from '@angular/core/testing';
import { ButtonAddComponent } from './button-add.component';

describe('ButtonAddComponent', () => {

  let component: ButtonAddComponent;
  let fixture: ComponentFixture<ButtonAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ButtonAddComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should start the counter on zero.', () => {
    expect(component.counter).toBe(0);
  });

  it('should emit 1 as the new counter value after calling the method add() once.', () => {
    let value_emitted = 0;

    component.newValue.subscribe( value => {
      value_emitted = value;
    });

    component.add();

    expect(value_emitted).toBe(1);
  });

  it('should emit 3 as the new counter value after calling the method add() three times.', () => {
    let value_emitted = 0;

    component.newValue.subscribe( value => {
      value_emitted = value;
    });

    component.add();
    component.add();
    component.add();

    expect(value_emitted).toBe(3);
  });

});
