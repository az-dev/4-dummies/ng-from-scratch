import { TestBed, ComponentFixture, fakeAsync, tick  } from '@angular/core/testing';

import { CounterComponent } from './counter.component';
import { ButtonAddComponent } from '../button-add/button-add.component';
import { ButtonSubstractComponent } from '../button-substract/button-substract.component';

describe('CounterComponent', () => {

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CounterComponent],
    }).compileComponents();
  });

  it('should start the counter on zero.', () => {
    const fixture = TestBed.createComponent(CounterComponent);
    const counter = fixture.componentInstance;

    expect(counter.counter).toBe(0);
  });

  it('should start with no error.', () => {
    const fixture = TestBed.createComponent(CounterComponent);
    const counter = fixture.componentInstance;

    expect(counter.error).toBeFalse();
  });

  it('should render the initial counter value.', () => {
    const fixture = TestBed.createComponent(CounterComponent);
    fixture.detectChanges();

    const compiled = fixture.nativeElement as HTMLElement;

    expect(compiled.querySelector('#counter')?.textContent).toContain('counter: 0');
  });
});

describe('CounterComponent Integration Testing', () => {

  let component: CounterComponent;
  let fixture: ComponentFixture<CounterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CounterComponent, ButtonAddComponent, ButtonSubstractComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CounterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should increment counter in one when clicking the #add button once.', () => {
    const compiled: HTMLElement = fixture.nativeElement;
    const counter = compiled.querySelector('#counter')!;
    const button_add: HTMLElement = fixture.debugElement.nativeElement.querySelector('#add');

    button_add.click();

    fixture.detectChanges();

    expect(counter.textContent).toContain('counter: 1');
  });

  it('should increment counter in three when clicking the #add button three times.', () => {
    const compiled: HTMLElement = fixture.nativeElement;
    const counter = compiled.querySelector('#counter')!;
    const button_add: HTMLElement = fixture.debugElement.nativeElement.querySelector('#add');

    button_add.click();
    button_add.click();
    button_add.click();

    fixture.detectChanges();

    expect(counter.textContent).toContain('counter: 3');
  });

  it('should show an error message after clicking the #substract button once.', () => {
    const compiled: HTMLElement = fixture.nativeElement;
    const button_substract: HTMLElement = fixture.debugElement.nativeElement.querySelector('#substract');

    button_substract.click();

    fixture.detectChanges();

    const error_message = compiled.querySelector('.alert')!;
    expect(error_message.textContent).toContain('counter cannot be negative.');
  });


  it('should NOT change counter after clicking the #substract button once.', () => {
    const compiled: HTMLElement = fixture.nativeElement;
    const counter = compiled.querySelector('#counter')!;
    const button_substract: HTMLElement = fixture.debugElement.nativeElement.querySelector('#substract');

    button_substract.click();

    fixture.detectChanges();

    expect(counter.textContent).toContain('counter: 0');
  });

  it('should have a counter value of 3.', () => {
    const compiled: HTMLElement = fixture.nativeElement;
    const counter = compiled.querySelector('#counter')!;
    const button_add: HTMLElement = fixture.debugElement.nativeElement.querySelector('#add');
    const button_substract: HTMLElement = fixture.debugElement.nativeElement.querySelector('#substract');

    button_add.click(); fixture.detectChanges();       // 1
    button_add.click(); fixture.detectChanges();       // 2
    button_add.click(); fixture.detectChanges();       // 3
    button_substract.click(); fixture.detectChanges(); // 2
    button_add.click(); fixture.detectChanges();       // 3
    button_add.click(); fixture.detectChanges();       // 4
    button_substract.click(); fixture.detectChanges(); // 3

    fixture.detectChanges();

    expect(counter.textContent).toContain('counter: 3');
  });
});
