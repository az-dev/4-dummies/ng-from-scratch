import { Component } from '@angular/core';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss']
})
export class CounterComponent {

  title: string = 'Angular From Scratch';
  counter: number = 0;
  error: boolean = false;

  constructor() { }

  handleValue(value: number): void {
    this.counter = value;
  }

  onError(value: boolean): void {
    this.error = value;
  }
}
